# Below is a demonstration of how to use pylegend lib service frame taking esgEmissionsData service.
# It assumes Legend engine running on localhost at 6300 port. There is no Auth here on local
# you can grab pylegend from pip
# pip install pylegend



import pylegend

tds_client = pylegend.LegendApiTdsClient(
    legend_client=pylegend.LegendClient(
        host="localhost",
        port=6300,
        secure_http=False
    )
)

frame = tds_client.legend_service_frame(
    service_pattern="/finos/esg/env/climate/getEmissionData",
    project_coordinates=pylegend.VersionedProjectCoordinates(
        group_id="org.finos.legend.esg",
        artifact_id="esg-reg-reporting",
        version="0.1.2"
    )
)

df = frame.execute_frame_to_pandas_df()
print(df)

dfteststring = frame.to_sql_query()
print(dfteststring)